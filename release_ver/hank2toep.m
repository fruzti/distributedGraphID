function T = hank2toep(H)

    c = H(end:-1:1,1);
    r = H(end,1:end);
    T = toeplitz(c,r);
    
end