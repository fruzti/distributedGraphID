%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Report: Distributed Graph Identification
% Author: Sundeep Prabhakar Chepuri, TU Delft
% Date: 08 Aug. 2017
%
% Script to compute all the eigenmodes and eigenfrequencies of a graph.
%

% Comments or bugs, e-mail to s.p.chepuri at tudelft.nl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;

typeGraph = 2;
nNodes = 50;

%% Graph Generation
% choose graph
switch typeGraph
    case 1
        G = gsp_sensor(nNodes);
    case 2
        G = gsp_random_regular(nNodes);
    case 3
        param.Nc = 10;
        G = gsp_community(nNodes,param);
end

N = G.N; % number of nodes
G = gsp_compute_fourier_basis(G);

% Laplacian
Ld = full(G.L);

% degree matrix
D= diag(diag(Ld));

% normalized Laplacian
L_norm = D^(-1/2)*Ld*D^(-1/2);


%% Generate data 

% Laplacian as Shift
Gshift = L_norm;

% eigendecomposition
[U, E] = eig(Gshift);
% E = diag([diag(E(end-2:end,end-2:end));zeros(N-3,1)]);
% Gshift = U*E*U';

M=N;   % Number of snapshots
X = randn(N,M); % seed input

% create sensing matrix
I = eye(N); ei = I(:,1);
Amat =[];
nShifts = N;
for ii = 1:nShifts
    Amat = [Amat; ei.'*Gshift^(ii-1)];
end

% Observations at node i
[Ux,~,Vx] = svd(X);
X = Ux*Vx';
Yi = Amat*X;

%% frequency estimation

Qmat = Yi*pinv(X);
Gmat = Qmat*Qmat.';

v_hist = nan(N);
eOffDig = nan(nShifts,1);

sVector = 3:1:nShifts;
strVect = {};
for nn = sVector

    try
        [v,~] = find_V(Gmat(1:nn,1:nn));
        vsr = sort(abs(v));
        Vest = vandermonde(vsr).';
        D_Gmat = pinv(Vest)*Gmat(1:nn,1:nn)*pinv(Vest');
    
        if length(vsr) > N
            vsr = vsr(1:N);
        end
    
        v_hist(1,nn) = vsr(1);
        v_hist(end-length(vsr)+2:end,nn) = vsr(2:end);
    
        Hmtx = D_Gmat - diag(diag(D_Gmat));
        eOffDig(nn) = norm(Hmtx,'fro')^2/norm(diag(diag(D_Gmat)),'fro')^2;

        strVect{nn} = strcat('#Shifts:',' ',num2str(nn));
    catch
        disp('Unstable Shift')
    end
end

p1 = stem(sort(abs(diag(E))),'-o','linewidth',1.1); hold on
% p2 = plot(v_hist(:,sVector),'-');
% ylim([0 max(diag(E))])
% legend(p2,strVect{sVector})

xlabel('Sorted Eigenvalues')
ylabel('Value')

meanV = nanmean(v_hist(:,sVector),2);
meanV(end) = nanmean(v_hist(end,sVector(sVector>(N-1))));
meanV(meanV>meanV(end)) = meanV(end);

p3 = stem(meanV,'dm','linewidth',1.1);
legend([p1;p3],{'True Spectra','Mean Spectra Over Shifts'})

figure,
semilogy(sVector,eOffDig(sVector))
xlabel('Num of Shifts')
ylabel('Diagonalization Error')

