
% parameter file...
rng(0)
ex.typeGraph = 0;                   % select type of graph to test
ex.nNodes = [];                     % set number of nodes
ex.typeShift = 1;                   % fix type of shift [Adj or L_norm]
ex.typeBase = 1;                    % type of graph
ex.showHankel = false;              % perfroms hankel result
ex.useMUSIC = false;                % unstable -not sure exactly why     
ex.plotEigModes = true;             % plot comparison of modes (fastest/slowest)
ex.paramplot.show_edges = 1;        % show edges
ex.plotGraph = false;               % show graph
ex.plotO = false;                   % plot Uhat.'*U
ex.svdV = false;                    % plot svd(V_true)
ex.svdTol = 1e-14;                  % select tol for truncation of SVD
ex.string = 'ex1';                  % name of example