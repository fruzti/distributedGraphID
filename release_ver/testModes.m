%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Report: Distributed Graph Identification
% Author: Sundeep Prabhakar Chepuri, TU Delft
% Date: 08 Aug. 2017
%
% Script to compute all the eigenmodes and eigenfrequencies of a graph.
%

% Comments or bugs, e-mail to s.p.chepuri at tudelft.nl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all; close all; clc;
%% Create the graph
% Adjaceny matrix
A = [0 1 0 1 0;
     1 0 1 0 0; 
     0 1 0 1 0; 
     1 0 1 0 1; 
     0 0 0 1 0];
D = (diag([2 2 2 3 1]'));
L = D- A;
N = size(A,1);
coords = [1 1; 2 0; 1 -1; 0 0; -2 0];

L_norm = D^(-1/2)*L*D^(-1/2);

Gshift = L_norm;


%% Generate data 

[U,E] = eig(Gshift); 
% E = diag(exp(-2j*pi/N *(0:N-1)));
Gshift = U*E*U';

M=N;   % Number of snapshots
X = randn(N,M); % seed input

nShift = 2*N;

% create sensing matrix
I = eye(N); ei = I(:,1);
Amat =[];
for ii = 1:nShift
    Amat = [Amat; ei.'*Gshift^(ii-1)];
end

% Observations at node i
[Ux,~,Vx] = svd(X);
X = Ux*Vx';
Yi = Amat*X;

% typical way for SI
hvec = Yi(1:2*N - 1,1);
H=hankel(hvec(1:N),hvec(N:2*N-1));
[vK, VK] = find_V(H);


% generate the toeplitz
T = toeplitz([Yi(1,1); zeros(N-1,1)], Yi(:,1));
T = T(:,N:end-1);
[Ut,Et] = eig(T);
%%
nShift = N;
Yi = Yi(1:N,:);

%% frequency estimation

% create selection matrix
aux = log(kron(exp([0:N-1]'),exp([0:N-1]')));
[~,ia1,ic1]=unique(floor(aux));
Jmat = eye(nShift^2); Jmat = Jmat(ia1,:);

Qmat = Yi*pinv(X);
Gmat = Qmat*Qmat';

[v,Vest] = find_V(Gmat);

%%
% number of modes (minus DC)
K = N-1; 

% projection matrix (we can simply remove the first measurement)
%P = eye(2*N-1) - [1;zeros(2*N-2,1)]*[1;zeros(2*N-2,1)]';
z_vec = Jmat*Gmat(:);
z_vec = z_vec(2:end);

% annihilation filter
Ns = length(z_vec);
Xl = toeplitz((z_vec(K:Ns-1,1)), z_vec(K:-1:1,1));
Xr = -(z_vec(K+1:Ns));
a = pinv(Xl)*Xr; root = roots([1 (a')]);

%root = sort([roots([1 (a')]);0]);
%[root sort(diag(E))]

%Matrix pencil followed by eigenvalue decomposition
X1 = toeplitz((z_vec(K:Ns-1,1)), z_vec(K:-1:1,1));
X2 = toeplitz((z_vec((K:Ns-1)+1,1)), z_vec((K:-1:1)+1,1));
D = diag(eig(pinv(X1)*X2));

% QZ method
[Tt,Ss, Qq, Zz] = qz(X1,X2)
eig(Ss,Tt)
diag(Ss)./diag(Tt)

% LS-SVD-ESPRIT
[~,~,C] = svd([X1,X2]);
C=C';
C1 = C(1:4,1:4); % C1= C(1:N-1, 1:N-1);
C2 = C(1:4,5:8); % C2 = C(1:N-1, N:end);
eig(pinv(C1)*C2);




%% Computing the eigenmodes

root_e = [root;0];
% construct the Vandermonde matrix
V = fliplr(vander(root_e)).';

Tmat = (pinv(V)*Qmat).'*(pinv(V)*Qmat);
[Uhat,~]= eig(Tmat); 

O = Uhat.'*U;
Uhat2 = Uhat*O
root_e