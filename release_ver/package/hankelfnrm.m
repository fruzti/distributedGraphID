function fnrm = hankelfnrm(col,row)
% fnrm = hankefnrm(col,row)
% computes the Frobenius norm of an m-by-n, m>=n,
% Hankel matrix given its first column and row.
% inputs
%   col    first column (dim=m) of a Hankel matrix
%   row    last row of (dim=n <= m) of a Hankel matrix
% output
%   fnrm   norm(hankel(col,row), 'fro')
m = length(col);
n = length(row);

ssq = 0.0;   % sum of squares of the entries
for k = 1:n
    ssq = ssq + k*real(col(k)'*col(k));
end
if m>n
    ssq = ssq + n*real(col(n+1:m)'*col(n+1:m));
end
for k=2:n
    ssq = ssq + (n-k+1)*real(row(k)'*row(k));
end
fnrm = sqrt(ssq);