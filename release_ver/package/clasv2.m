function [smin,smax,sr,cr,sl,cl]=clasv2(f,g,h)
%
% Compute singular value decomposition of 2x2 
% upper triangular matrix (complex):
%	[f g] = [sign(f)    0   ]*[cl -sl'].[smax  0  ].[cr -sr']'
%	[0 h]   [   0    sign(h)] [sl  cl'] [ 0   smin] [sr  cr']
% smax is larger singular value and smin is
% smaller singular value.
%
% Dependency: slasv2.m
%
% S. Qiao	McMaster Univ.	Nov. 1993
%
[smin,smax,sr,cr,sl,cl] = slasv2(abs(f), abs(g), abs(h));

if (f==0), fs= 1; else fs= sign(f); end;
if (g==0), gs= 1; else gs= sign(g); end;
ssign = fs*gs';
sl = sl*ssign;
sr = sr*ssign;
