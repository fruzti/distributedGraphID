%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Report: Distributed Graph Identification
% Author: M.Coutino - S.P. Chepuri, TU Delft
% Date: Oct. 2017
%
% Script to compute all the eigenmodes and eigenfrequencies of a graph.
%

% Comments or bugs, e-mail to m.a.coutinominguez at tudelft.nl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
clear all;
close all;
clc;
rng(5);
% different conf. files for images (loads ex)
% run ex1_params.m              %toy example
 run ex2_params.m              %karate graph
%run ex3_params.m                %community graph


fprintf('Topology ID for Graphs using Aggregation Sampling...\n')
fprintf('Coutino-Chepuri-Leus <=> TUDELFT 2017\n')
fprintf('+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n')

shiftTypes = {'complex','uniform','real'}; % From Adnan Gavili's paper.
baseShift = {'laplacian_norm','adjacency'};

%% Graph Generation
% choose graph
switch ex.typeGraph
    case 0
        fprintf('-Toy Graph Example, N=5...\n')
        A = [0 1 0 1 0;
            1 0 1 0 0; 
            0 1 0 1 0; 
            1 0 1 0 1; 
            0 0 0 1 0];
        coords = [1 1; 2 0; 1 -1; 0 0; -2 0];
        G = gsp_graph(A,coords);
    case 1
        fprintf('-Sensor Graph Example, N=%d...\n',ex.nNodes)
        G = gsp_sensor(ex.nNodes);
    case 2
        fprintf('-Regular Random Graph Example, N=%d...\n',ex.nNodes)
        G = gsp_random_regular(ex.nNodes);
    case 3
        fprintf('-Community Graph Example, N=%d...\n',ex.nNodes)
        param.Nc = 5;
        G = gsp_community(ex.nNodes,param);
    case 4
        fprintf('-Karate Graph Example, N=34...\n')
        run karate_adjacency.m
        run karate_coordinates.m
        G = gsp_graph(A,coord);
end

N = G.N; % number of nodes
G = gsp_compute_fourier_basis(G);

if (~isempty(G.coords) & ex.plotGraph)
    gsp_plot_graph(G,ex.paramplot);
    view(-37.5,30)
end
% Laplacian
Ld = full(G.L);

% degree matrix
D= diag(diag(Ld));

% normalized Laplacian
L_norm = D^(-1/2)*Ld*D^(-1/2);

%% Generate data 
switch baseShift{ex.typeBase}
    case 'laplacian_norm'
        fprintf('-Shift Base: L_norm...\n')
        Gshift = L_norm;
    case 'adjacency'
        fprintf('-Shift Base: A...\n')
        Gshift = double(full(G.A));
end

[U,E] = eig(Gshift); 

switch shiftTypes{ex.typeShift}
    case 'complex'
        fprintf('-Complex Shift S_phi...\n')
        E = diag(exp(-1j*2*pi*rand(N,1)));
    case 'uniform'
        fprintf('-Uniform Complex Shift S_e...\n')
        E = diag(exp(-2j*pi/N *(0:N-1)));
    otherwise
        fprintf('-Real Shift S...\n')
end

Gshift = U*E*U';

nShift = 2*N;       % number of shift to collect
M=nShift;           % Number of snapshots

% create sensing matrix
I = eye(N); ei = I(:,N);
Amat =[];
for ii = 1:nShift
    Amat = [Amat; ei.'*Gshift^(ii-1)];
end

X = randn(N,M);             % seed input
[Ux,~,Vx] = svd(X);
X = Ux*Vx(1:nShift,1:N)';   % well condition seed

% Observations at node i
Yi = Amat*X;
%% hankel method
if (ex.showHankel)
    fprintf('-Solving Hankel Dec. (Laczos)...\n')
    hvec = Yi(1:2*N - 1,1);
    H=hankel(hvec(1:N),hvec(N:2*N-1));
    [Ehk_hat, Vk] = find_V(H);
    if ( strcmp(shiftTypes{ex.typeShift},'real') )
        Ehk_hat = real(Ehk_hat);
    else
        Ehk_hat = Ehk_hat./abs(Ehk_hat);
    end
end
%% music method -for complex shifts
if ( (ex.typeShift == 1 || ex.typeShift == 2) && ex.useMUSIC )
    fprintf('-Solving using Music Method...\n')
    [Uy,~,~] = eig(Yi*Yi');

    U_perp = Uy(:,N+1:end);

    nAngles = 1e3;
    angle2Test = deg2rad(linspace(-180,180,nAngles));
    f_music = nan(1,nAngles);

    for i = 1:nAngles
        a_vec = exp(-1j*angle2Test(i)*(0:nShift-1)).';
    
        f_music(i) = (1/norm(U_perp'*a_vec)^2);
    end

    figure,
    p1 = semilogy(angle2Test, f_music,'-r'); hold on,
    p2 = stem(angle(diag(E)),1e3*ones(N,1),'ob');
    legend([p1;p2],{'MUSIC','True Phases'})
    xlabel('Angle [rad]')
    ylabel('Pseudospectrum')
    
end

%% pencil method using QZ
fprintf('-Solving QZ Method...\n')
Y0 = zeros(N);
Y1 = zeros(N);

for ii = 0:N-1
    Y0(:,N-ii) = Yi(ii+1:N+ii,1);
    Y1(:,N-ii) = Yi(ii+2:N+ii+1,1);
end

% QZ method
[Tt,Ss, Qq, Zz] = qz(Y0,Y1);
eig(Ss,Tt);

E_hat = diag(Ss)./diag(Tt);
E_hat_tr = diag(diag(Ss))*pinv(diag(diag(Tt)),ex.svdTol);

if ( strcmp(shiftTypes{ex.typeShift}, 'real') )
    E_hat = real(E_hat);
    E_hat_tr = real(E_hat_tr);
else
    E_hat = E_hat./abs(E_hat);
    E_hat_tr = E_hat_tr./abs(E_hat_tr);
end

%% plot output
fprintf('-Plotting Roots...\n')
if ex.typeShift == 3
    p1 = stem(sort(diag(E),'descend'),'ob'); hold on
    p2 = stem(sort(E_hat,'descend'),'xr');
    if ex.showHankel
        p3 = stem(sort(Ehk_hat,'descend'),'dg');
        legend([p1;p2;p3],{'True','Est. Pencil','Est. Hankel'})
    else
        legend([p1;p2],{'True','Estimates'},'Location', 'best')
    end
    xlabel('Index [i] ')
    ylabel('Eigenvalue [\lambda]')
    set(gca,'FontSize',16); legend boxoff;
    print('-depsc', strcat('figures/graph_ID_QZreal_',ex.string,'.eps'));
    
else
    figure,
    p1 = plot(real(diag(E)),imag(diag(E)), 'ob','MarkerSize',10); hold on
    p2 = plot(real(E_hat),imag(E_hat),'xr','MarkerSize',8);

    if ex.showHankel
        p3 = plot(real(Ehk_hat),imag(Ehk_hat),'dg');
        legend([p1;p2;p3],{'True','Est. Pencil','Est. Hankel'})
    else
        legend([p1;p2],{'True','Estimates'},'Location','best');
    end
    axis off, axis square
    set(gca,'FontSize',16); legend boxoff; 
    print('-depsc', strcat('figures/graph_ID_QZ_',ex.string,'.eps'));
    
    figure,
    p4 = plot(real(diag(E)),imag(diag(E)),'ob','MarkerSize',10);hold on,
    p5 = plot(real(diag(E_hat_tr)),imag(diag(E_hat_tr)),'*k','MarkerSize',8);
    legend([p4;p5],{'True','Estimates (rank reduction)'},...
        'Position',[0.372767857142857 0.585714285714286 ...
    0.275892857142857 0.094047619047619]);
    axis off, axis square
    set(gca,'FontSize',16); legend boxoff;
    print('-depsc', strcat('figures/graph_ID_QZt_',ex.string,'.eps'));
end
%% find Modes -QZ Result (Only)
V_hat = vandermonde(E_hat,N).';
%
V_true = vandermonde(diag(E)).';
%
Qmat = Yi(1:N,:)*pinv(X);
Tmat = (pinv(V_hat)*Qmat).'*(pinv(V_hat)*Qmat);
[Uhat,~]= svd(Tmat); 

O = Uhat'*U;
Uhat2 = Uhat*O;
%% plot extra results
% spy image for O
if(ex.plotO)
    figure, spy(abs(O)>= 1e-2,'k'); xlabel('');
    line([0 35],[23.5 23.5],'color','red','linewidth',2)
    set(gca,'FontSize',16); 
    print('-depsc', strcat('figures/graph_ID_O_',ex.string,'.eps'));
end 
% svd of true V 
if(ex.svdV)
    figure, semilogy(sort(svd(Y1),'descend'),'k*'); 
    hold on,
    line([23.5 23.5],[1e-20 1e5],'color','red','linewidth',2)
%     figure, semilogy(abs(sort(diag(Tt),'descend')),'*'); 
    xlabel('Node index')
    ylabel('singular values')
    set(gca,'FontSize',16); legend boxoff
    print('-depsc', strcat('figures/graph_ID_svd_',ex.string,'.eps'));
end
%% plot w/ diff colors for pos & neg
if ex.plotEigModes   % real things only...
    fprintf('-Plotting Modes...\n')
    figure,
%     subplot(2,2,1)                  % original U
    gsp_plot_graph(G,ex.paramplot);  
    hold on, 
    i_posS = U(:,1) >= 0; i_posF = U(:,end) >= 0;
    i_negS = U(:,1) <  0; i_negF = U(:,end) <  0;
    if ~isempty(U(i_posS,1))
        stem3(G.coords(i_posS,1),G.coords(i_posS,2),U(i_posS,1)',...
        'Color',[0 0 0],'MarkerSize',10,'LineWidth',1.5);
    end
    if ~isempty(U(i_negS,1))
        stem3(G.coords(i_negS,1),G.coords(i_negS,2),U(i_negS,1)',...
            'Color',[1 0 0],'MarkerSize',10,'LineWidth',1.5);
    end
    view(-90,10)
    zlim([-1 1])
    print('-depsc', strcat('figures/graph_ID_U1_',ex.string,'.eps'));
    
%     subplot(2,2,2)                  % original U
    figure,
    gsp_plot_graph(G,ex.paramplot);
    hold on, 
    if ~isempty(U(i_posF,end))
        stem3(G.coords(i_posF,1),G.coords(i_posF,2),U(i_posF,end)',...
            'Color',[0 0 0],'MarkerSize',10,'LineWidth',1.5);
    end
    if ~isempty(U(i_negF,end))
        stem3(G.coords(i_negF,1),G.coords(i_negF,2),U(i_negF,end)',...
            'Color',[1 0 0],'MarkerSize',10,'LineWidth',1.5);
    end
    view(67,17)
    zlim([-1 1])
    print('-depsc', strcat('figures/graph_ID_U2_',ex.string,'.eps'));
    
    [~,ix] = max(abs(O));           % retrieve frequency ordering 
%     subplot(2,2,3)                  % estimated Uhat
    figure,
    gsp_plot_graph(G,ex.paramplot);
    hold on, 
    i_posS = Uhat(:,ix(1)) >= 0; i_posF = Uhat(:,ix(end)) >= 0;
    i_negS = Uhat(:,ix(1)) <  0; i_negF = Uhat(:,ix(end)) <  0;
    if ~isempty(Uhat(i_posS,ix(1)))
        stem3(G.coords(i_posS,1),G.coords(i_posS,2),real(Uhat(i_posS,ix(1)))',...
         'Color',[0 0 0],'Marker','+','MarkerSize',10,'LineWidth',1.5);
    end
    if ~isempty(Uhat(i_negS,ix(1)))
        stem3(G.coords(i_negS,1),G.coords(i_negS,2),real(Uhat(i_negS,ix(1)))',...
        'Color',[1 0 0],'Marker','+','MarkerSize',10,'LineWidth',1.5);
    end
    view(-90,10)
    zlim([-1 1])
    print('-depsc', strcat('figures/graph_ID_Uhat1_',ex.string,'.eps'));
    
%     subplot(2,2,4)
    figure,
    gsp_plot_graph(G,ex.paramplot);
    hold on, 
    if ~isempty(Uhat(i_posF,ix(end)))
        stem3(G.coords(i_posF,1),G.coords(i_posF,2),real(Uhat(i_posF,ix(end)))',...
            'Color',[0 0 0],'Marker','+','MarkerSize',10,'LineWidth',1.5);
    end
    if ~isempty(Uhat(i_negF,ix(end)))
        stem3(G.coords(i_negF,1),G.coords(i_negF,2),real(Uhat(i_negF,ix(end)))',...
            'Color',[1 0 0],'Marker','+','MarkerSize',10,'LineWidth',1.5);
    end
    view(67,17)
    zlim([-1 1])
    print('-depsc', strcat('figures/graph_ID_Uhat2_',ex.string,'.eps'));
end
%% error btw estimation
e_r = sum(svd(U).^2 - 2*svd(Uhat'*U) + svd(Uhat).^2);
fprintf('-Error ||U - U_hat*Q||_F: %f\n',e_r);
%% resets seed
rng('default')
%close all
%% error w/ Golumb

% % %%
% 
% 
% figure;
% [auxX, auxY] = gplot(A,coord);
% plot(auxX, auxY, '-o',  'LineWidth',1.5,...
%                        'MarkerEdgeColor','r',...
%                        'MarkerFaceColor','r',...
%                        'MarkerSize',8);
% hold on;                   
% plot(coord(:,1),coord(:,2),'ok',...
%     'MarkerSize',15,'LineWidth',1.5);
% axis square; axis off;
% set(gca,'FontSize',16);
% title('Sample 4 out of 34 nodes')
% print('-depsc', 'graph_sampling_par.eps');