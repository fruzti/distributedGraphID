function H = toep2hank(T)

    H = hankel(flip(T(:,1)),(T(1,1:end)));

end