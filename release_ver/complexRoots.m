%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Report: Distributed Graph Identification
% Author: Sundeep Prabhakar Chepuri, TU Delft
% Date: 08 Aug. 2017
%
% Script to compute all the eigenmodes and eigenfrequencies of a graph.
%

% Comments or bugs, e-mail to s.p.chepuri at tudelft.nl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;

typeGraph = 2;
nNodes = 20;

%% Graph Generation
switch typeGraph
    case 1
        G = gsp_sensor(nNodes);
    case 2
        G = gsp_random_regular(nNodes);
    case 3
        G = gsp_community(nNodes);
end
% paramplot.show_edges = 1;
% gsp_plot_graph(G,paramplot);

N = G.N; % number of nodes
G = gsp_compute_fourier_basis(G);

% Laplacian
Ld = full(G.L);

% degree matrix
D = diag(diag(Ld));

% normalized Laplacian
L_norm = D^(-1/2)*Ld*D^(-1/2);


%% Generate data 

% eigendecomposition
[U, ~] = eig(Ld);

rts = randn(N,1) + 1j*randn(N,1);
rts = rts./abs(rts);
E = diag(rts);

Gshift = U*E*U';

M=N;   % Number of snapshots
X = randn(N,M); % seed input

% create sensing matrix
I = eye(N); ei = I(:,1);
Amat =[];
nShifts = N;
for ii = 1:nShifts
    Amat = [Amat; ei.'*Gshift^(ii-1)];
end
%% frequency estimation

% Observations at node i
[Ux,~,Vx] = svd(X);
X = Ux*Vx';
Yi = Amat*X;

Qmat = Yi*pinv(X);
Gmat = Qmat*Qmat.';

[v,Vest] = find_V(Gmat);
v = v./abs(v);

scatter(real(rts),imag(rts),'ob'), hold on,
scatter(real(v), imag(v),'xr')

% number of modes (minus DC)
aux = log(kron(exp([0:N-1]'),exp([0:N-1]')));
[~,ia1,ic1]=unique(floor(aux));
Jmat = eye(nShifts^2); Jmat = Jmat(ia1,:);
K = N-1; 

% projection matrix (we can simply remove the first measurement)
%P = eye(2*N-1) - [1;zeros(2*N-2,1)]*[1;zeros(2*N-2,1)]';
z_vec = Jmat*Gmat(:);
z_vec = z_vec(2:end);

% annihilation filter
Ns = length(z_vec);
Xl = toeplitz((z_vec(K:Ns-1,1)), z_vec(K:-1:1,1));
Xr = -(z_vec(K+1:Ns));
a = pinv(Xl)*Xr; root = roots([1 (a')]);

scatter(real(root),imag(root),'xg')