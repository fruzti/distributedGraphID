
%parameter file 2
rng(0);             % fixes seed

ex.typeGraph = 4;               % select type of graph to test
ex.nNodes = [];                 % set number of nodes
ex.typeShift = 1;               % fix type of shift
ex.typeBase = 2;                % type of graph  [L_norm, Adj]
ex.showHankel = false;          % perfroms hankel result
ex.useMUSIC = false;            % unstable -not sure exactly why     
ex.plotEigModes = true;         % plot comparison of modes (fastest/slowest)
ex.paramplot.show_edges = 1;    % show edges
ex.plotGraph = false;           % show graph
ex.plotO = true;                % plot Uhat.'*U
ex.svdV = true;                 % plot svd(V_true)
ex.svdTol = 1e-14;              % select tol for truncation of SVD
ex.string = 'ex2';              % name of example