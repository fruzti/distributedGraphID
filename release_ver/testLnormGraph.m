%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Report: Distributed Graph Identification
% Author: Sundeep Prabhakar Chepuri, TU Delft
% Date: 08 Aug. 2017
%
% Script to compute all the eigenmodes and eigenfrequencies of a graph.
%

% Comments or bugs, e-mail to s.p.chepuri at tudelft.nl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;

%% Graph Generation

G = gsp_random_regular(20);
% paramplot.show_edges = 1;
% gsp_plot_graph(G,paramplot);

N = G.N; % number of nodes
G = gsp_compute_fourier_basis(G);

% Laplacian
Ld = full(G.L);

% degree matrix
D= diag(diag(Ld));

% normalized Laplacian
L_norm = D^(-1/2)*Ld*D^(-1/2);

% eigendecomposition
[U, E] = eig(Ld);
% E(1:3,1:3) = zeros(3);
% L_norm = U*E*U';
% E = diag(linspace(0,3,N));
%%
% s = [1 2 1 3 2 3 3 3];
% t = [2 1 3 1 3 4 5 6];
% G = digraph(s,t);
% A = full(adjacency(G));
% [Ua,Ea]= eig(A*A');
%% Generate data 
Gshift = Ld;
% Gshift = G.A;
% [U, E] = eig(double(full(Gshift)));
% E = randn(N,1) + 1j*randn(N,1);
% E = E./abs(E);

% Gshift = U*diag(E)*U';

M=N;   % Number of snapshots
X = randn(N,M); % seed input

% create sensing matrix
I = eye(N); ei = I(:,1);
Amat =[];
nShifts = N;
for ii = 1:nShifts
    Amat = [Amat; ei.'*Gshift^(ii-1)];
end

% Observations at node i
[Ux,~,Vx] = svd(X);
X = Ux*Vx';
Yi = Amat*X;

%% frequency estimation

Qmat = Yi*pinv(X);
Gmat = Qmat*Qmat.';

[v,Vest] = find_V(Gmat);

D_Gmat = inv(Vest)*Gmat*inv(Vest');

stem(sort(abs(diag(E))),'ob'), hold on
vsr = sort(abs(v));
stem([1 N-nShifts+1:N],sort([vsr(1); vsr(end-nShifts+1:end)]),'xr')
eOffDig = norm(D_Gmat - diag(diag(D_Gmat)),'fro');
fprintf('Error OffDiag: %f\n\n',eOffDig)
%%

% create selection matrix
aux = log(kron(exp([0:N-1]'),exp([0:N-1]')));
[~,ia1,ic1]=unique(floor(aux));
Jmat = eye(nShifts^2); Jmat = Jmat(ia1,:);

% number of modes (minus DC)
K = N-1; 

% projection matrix (we can simply remove the first measurement)
%P = eye(2*N-1) - [1;zeros(2*N-2,1)]*[1;zeros(2*N-2,1)]';
z_vec = Jmat*Gmat(:);
z_vec = z_vec(2:end);

% annihilation filter
Ns = length(z_vec);
Xl = toeplitz((z_vec(K:Ns-1,1)), z_vec(K:-1:1,1));
Xr = -(z_vec(K+1:Ns));
a = pinv(Xl)*Xr; root = roots([1 (a')]);

%root = sort([roots([1 (a')]);0]);
%[root sort(diag(E))]

%Matrix pencil followed by eigenvalue decomposition
X1 = toeplitz((z_vec(K:Ns-1,1)), z_vec(K:-1:1,1));
X2 = toeplitz((z_vec((K:Ns-1)+1,1)), z_vec((K:-1:1)+1,1));
D = diag(eig(pinv(X1)*X2));

% QZ method
[Tt,Ss, Qq, Zz] = qz(X1,X2);
eig(Ss,Tt);
Tt(max(Tt(:)) == Tt) = 1;
root = diag(Ss)./diag(Tt);

% LS-SVD-ESPRIT
[~,~,C] = svd([X1,X2]);
C=C';
C1 = C(1:4,1:4); % C1= C(1:N-1, 1:N-1);
C2 = C(1:4,5:8); % C2 = C(1:N-1, N:end);
eig(pinv(C1)*C2);


% %% Computing the eigenmodes
% 
root_e = [root;0];
% % construct the Vandermonde matrix
% V = fliplr(vander(root_e)).';
% 
% Tmat = (pinv(V)*Qmat).'*(pinv(V)*Qmat);
% [Uhat,~]= eig(Tmat); 
% 
% O = Uhat.'*U;
% Uhat2 = Uhat*O;

%%
close all,
p1 = stem(sort(abs(diag(E))),'ob'); hold on
vsort = sort(abs(v(1:N)));
% p2 = stem([vsort(1:end-1); v(end-1)],'xr');
p2 = stem(vsort,'xr');
% p2 = stem(sort(abs([v(1:end-1); v(end-1)])),'xr');
sroots = sort(abs(root_e));
p3 = stem(sroots,'dg');
p4 = stem(0.5*(sroots+vsort(1:N)),'sm');

legend([p1;p2;p3],{'True','Lacsoz','Prony'})