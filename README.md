
Code for paper: "DISTRIBUTED ANALYTICAL GRAPH IDENTIFICATION"

Conference: ICASSP 2018, Calgary

Autors: S.P. Chepuri(!), M. Coutino(!), A. Marques(!!), G. Leus(!).

Affilations: (!) TU Delft (!!) King Juan Carlos University

Contact: m.a.coutinominguez@tudelf.nl


Requires GSP Toolbox (https://lts2.epfl.ch/gsp/)


Instructions: To run the code simply execute mainFile.m
